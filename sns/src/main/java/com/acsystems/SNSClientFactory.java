package com.acsystems.aws.sns;

import com.acsystems.aws.sns.impl.SNSClientImpl;

public class SNSClientFactory {

    public static SNSClient build() {
        return new SNSClientImpl();
    }
}