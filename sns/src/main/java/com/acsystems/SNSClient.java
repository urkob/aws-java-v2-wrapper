package com.acsystems.aws.sns;

import java.util.concurrent.*;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

/**
 * SNSClient
 */
public interface SNSClient {
    PublishRequest buildPublishMessage(String topicARN, String message);

    CompletableFuture<PublishResponse> publishMessageAsync(PublishRequest publishRequest);

}