package com.acsystems.aws.sns.impl;

import com.acsystems.aws.sns.SNSClient;
import java.util.concurrent.*;

import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.http.crt.AwsCrtAsyncHttpClient;

import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.SnsAsyncClientBuilder;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

public class SNSClientImpl implements SNSClient {

    private SnsAsyncClient snsClient = null;

    public SNSClientImpl() {
        this.initSNSClient(null);
    }

    public SNSClientImpl(int maxConcurrency) {
        this.initSNSClient(maxConcurrency);
    }

    private void initSNSClient(Integer maxConcurrency) {
        this.snsClient = SnsAsyncClient.builder()
                .httpClientBuilder(
                        AwsCrtAsyncHttpClient.builder().maxConcurrency(maxConcurrency != null ? maxConcurrency : 50))
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create()).build();
    }

    @Override
    public PublishRequest buildPublishMessage(String topicARN, String message) {
        return PublishRequest.builder().message(message).topicArn(topicARN).build();
    }

    @Override
    public CompletableFuture<PublishResponse> publishMessageAsync(PublishRequest publishRequest) {
        return snsClient.publish(publishRequest);
    }

}
